package com.headhunter.joboffer;

import java.util.Calendar;


import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.content.DialogInterface;
import android.content.Intent;

public class CvInput extends Activity {

	private int mYear;
	private int mMonth;
	private int mDay;
	private String calendarDate;

	private Button mPickDate;
	private TextView textview_isSelected;
	private Button sendCV;

	static final int DATE_DIALOG_ID = 0;
	  
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cv_input);
        
     Spinner spinner = (Spinner) findViewById(R.id.spinner);
     ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
     R.array.gender, android.R.layout.simple_spinner_item);
     adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
     spinner.setAdapter(adapter);
     
             
     mPickDate = (Button) findViewById(R.id.myDatePickerButton);

     mPickDate.setOnClickListener(new View.OnClickListener() {
         public void onClick(View v) {
             showDialog(DATE_DIALOG_ID);
         }
     });
     
     
     // get the current date
     final Calendar c = Calendar.getInstance();
     mYear = c.get(Calendar.YEAR);
     mMonth = c.get(Calendar.MONTH);
     mDay = c.get(Calendar.DAY_OF_MONTH);

 
     textview_isSelected = (TextView) findViewById(R.id.textView_date_isSelected);
     textview_isSelected.setText("�� �����������");
     
     String replyText = getIntent().getStringExtra("message");
     if (replyText != null) {
    	if (replyText.length() == 0) return;
    	
     new AlertDialog.Builder(this)
     .setTitle("����� ���������")
     .setMessage(replyText)
     .setPositiveButton("OK", new DialogInterface.OnClickListener() {
         public void onClick(DialogInterface dialog, int which) { 
             // continue with delete
         }
      })
      .show();
     
     	}
    }


    public void butSendCV_Click(View v){
    	 Intent intent = new Intent(CvInput.this, Employer.class);
 		
    	    TextView txtName = (TextView)findViewById(R.id.editText_name);
			intent.putExtra("name", txtName.getText().toString());
			
			if (calendarDate != null) {
			intent.putExtra("birth", calendarDate);
			}
			
			Spinner txtSex = (Spinner)findViewById(R.id.spinner);
			String TextSpinner = txtSex.getSelectedItem().toString();
			intent.putExtra("sex", TextSpinner);
			
			TextView txtPosition = (TextView)findViewById(R.id.editTextPosition);
			intent.putExtra("position", txtPosition.getText().toString());
			
			TextView txtSalary = (TextView)findViewById(R.id.editText2);
			intent.putExtra("salary", txtSalary.getText().toString());

			TextView txtPhone = (TextView)findViewById(R.id.editText3);
			intent.putExtra("phone", txtPhone.getText().toString());
			
			TextView txtEmail = (TextView)findViewById(R.id.editText4);
			intent.putExtra("email", txtEmail.getText().toString());
     
     	    startActivity(intent);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cv_input, menu);
        return true;
    }
    


    private DatePickerDialog.OnDateSetListener mDateSetListener =
        new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, 
                                  int monthOfYear, int dayOfMonth) {
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;
                
                updateDateText();
            }

        };
 
       
        private void updateDateText() {
        	
        	textview_isSelected.setText(
                new StringBuilder()
                        .append(mDay).append("/")
                        .append(mMonth +1).append("/")
                        .append(mYear).append(" "));
        	
        	calendarDate = textview_isSelected.getText().toString();
        }
   
    protected Dialog onCreateDialog(int id) {
       switch (id) {
       case DATE_DIALOG_ID:
          return new DatePickerDialog(this,
                    mDateSetListener,
                    mYear, mMonth, mDay);
       }
       return null;
    }
    
}
