package com.headhunter.joboffer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

public class Employer extends Activity 
{
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.employer);
        
        Intent intent = getIntent();
        
        String user = intent.getStringExtra("name");
        String birth = intent.getStringExtra("birth");
        String salary = intent.getStringExtra("salary");
        String sex = intent.getStringExtra("sex");
        String position = intent.getStringExtra("position");
        String phone = intent.getStringExtra("phone");
        String email = intent.getStringExtra("email");
        
        TextView txtInfo = (TextView)findViewById(R.id.content_name);
        txtInfo.setText(user);
        
        TextView txtBirth = (TextView)findViewById(R.id.content_birth);
        txtBirth.setText(birth);
        
        TextView txtSex = (TextView)findViewById(R.id.content_sex);
        txtSex.setText(sex);
        
        TextView txtSalary = (TextView)findViewById(R.id.content_salary);
        txtSalary.setText(salary);
        
        TextView txtPosition = (TextView)findViewById(R.id.content_position);
        txtPosition.setText(position);
        
        TextView txtPhone = (TextView)findViewById(R.id.content_phone);
        txtPhone.setText(phone);
        
        TextView txtEmail = (TextView)findViewById(R.id.content_email);
        txtEmail.setText(email);
       
    }
    
    public void butReply_Click(View v){
   	 Intent intent = new Intent(Employer.this, CvInput.class);
		
   	    	TextView txtMessage = (TextView)findViewById(R.id.input_reply);
			intent.putExtra("message", txtMessage.getText().toString());

    	    startActivity(intent);
   }
}